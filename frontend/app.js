var vm = new Vue({
    el: '#base',
    data: {
        lights: []
    },

    // Anything within the mounted function will run when the app loads
    mounted: function() {
        console.log("Ready!")
        this.$http.get(this.$http.root).then(response => {
            this.$data.lights = response.body.items;
        }, error => {
            console.warn("Couldn't fetch lights");
            console.warn(error)
        })
    },

    methods: {
        toggle: function(index) {
            var on = this.$data.lights[index].on;
            this.$data.lights[index].on = !on;
            light = this.$data.lights[index];
            this.$http.patch(light.uri, light).then(response => {
                console.log("Toggled " + light.name);
            }, error => {
                console.warn("tusan");
            })
        },
        allOff: function() {
            length = this.$data.lights.length
            for (var i = 0; i < length; i++) {
                light = this.$data.lights[i]
                if (light.on) {
                    light.on = false;
                    this.$http.patch(light.uri, light).then(response => {
                        console.log("Turned off " + light.name)
                    }, response => {
                        console.warn("Got an error: " + response)
                    })
                }
            }
        }
    },
    http: {
        root: 'http://betta.local:5000/husli/lights',
        headers: {
            Authorization: 'Basic ZWx2YTp0b2x2'
        }
    }
});
