A small REST API for controlling both Hue lights and Telldus devices
====================================================================

The API gives a unified way to control lights over HTTP. It also comes with a frontend that displays lights, their state and lets the user turn them on and off.

Requirements
------------

Backend
^^^^^^^

* Python 3.4+ (only tested on 3.4)
* tellcore-py
    * Install with ``$ pip install tellcore-py``
    * tellcore-py requries `Telldus Core library <http://telldus.com/products/nativesoftware>`
* phue
    * Install with ``$ pip install phue``

Frontend
^^^^^^^^
* Vye
    * ``$ npm install vue``
* Vye-resources
    * ``$ npm install vye-resources``

Todo
----

* Add the ability to set the color and brightness on Hue lights
* Make this a proper package for easier installation
* Add an actual usages section in the README
* Add a "turn off all lights button" to the frontend

Done
^^^^

* Add the actual frontend