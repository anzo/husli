#!/usr/bin/env python3

from phue import Bridge, PhueRegistrationException
from time import sleep
import requests

# Perform a manual nupnp lookup since the phue lib is stupid.
nupnp_response = requests.get('http://www.meethue.com/api/nupnp')
parsed_content = nupnp_response.json()

try:
    bridge_ip = parsed_content[0]['internalipaddress']
except:
    raise Exception('Unable to locate Hue bridge')

connected = False
attempts_left = 5

while not connected:
    try:
        bridge = Bridge(ip=bridge_ip)
        print('Connected to Hue bridge running at {0}'.format(bridge_ip))
        connected = True
    except PhueRegistrationException:
        if attempts_left == 0:
            raise

        print('Link button has not been pressed during the last 30 seconds. '
              '({0} attempts remaining)'.format(attempts_left))

        sleep(15)
        attempts_left -= 1

bridge.connect()
bridge.get_api()


def get_devices():
    devices = []
    for light in bridge.lights:
        devices.append(format_light(light))

    return devices


def get_device(id):
    try:
        return format_light(bridge[id])
    except KeyError:
        return None


def update_device(device):
    light = bridge[device['id']]
    light.on = device['on']


def format_light(light):
    l = {}
    l['name'] = light.name
    l['on'] = light.on
    l['id'] = light.light_id
    l['type'] = 'hue'
    return l

if __name__ == '__main__':
    print(get_devices())
    print()
    print(get_device(1))
