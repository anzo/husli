#!/usr/bin/env python3

from tellcore.telldus import TelldusCore
import tellcore.constants as constants

core = TelldusCore()


def get_devices():
    devices = []
    for device in core.devices():
        devices.append(_format_device(device))

    return devices


def get_device(id):
    device = _get_device(id)
    if device is None:
        return None
    else:
        return _format_device(device)


def update_device(device):
    found_device = _get_device(device['id'])
    if found_device is None:
        return None
    else:
        if device['on']:
            found_device.turn_on()
        else:
            found_device.turn_off()


def _get_device(id):
    devices = core.devices()
    found_devices = [d for d in devices if d.id == id]
    if found_devices:
        return found_devices[0]
    else:
        return None


def _format_device(device):
    d = {}
    d['id'] = device.id
    d['name'] = device.name
    d['on'] = _get_device_state(device)
    d['type'] = u'telldus'
    return d


def _get_device_state(device):
    last_command = device.last_sent_command(constants.TELLSTICK_TURNON)
    if last_command == constants.TELLSTICK_TURNON:
        return True
    else:
        # We don't care if it actually is TELLSTICK_TURNOFF,
        # this is a reasonable default
        return False

if __name__ == '__main__':
    print(get_devices())
    print()
    print(get_device(1))
    update_device(2)
