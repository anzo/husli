#!/usr/bin/env python3

import sys
import ssl

from flask import Flask, jsonify, abort, make_response, request, url_for
from flask.ext.httpauth import HTTPBasicAuth
from flask.ext.cors import CORS

import telldus
import hue

app = Flask(__name__)
auth = HTTPBasicAuth()
CORS(app)


@app.route('/husli/lights/', methods=['GET'])
@auth.login_required
def get_lights():
    lights = []
    lights = lights + telldus.get_devices()
    lights = lights + hue.get_devices()
    return jsonify(items=[_make_public_light(l) for l in lights])


@app.route('/husli/lights/<string:type>/<int:id>/', methods=['GET'])
@auth.login_required
def get_light(type, id):
    light = _get_light(type, id)
    if light is None:
        abort(404)
    return jsonify(_make_public_light(light))


@app.route('/husli/lights/<string:type>/<int:id>/', methods=['PATCH'])
@auth.login_required
def update_light(type, id):
    light = _get_light(type, id)
    if light is None:
        abort(404)

    if not request.json:
        return _invalid_request_response("Must be JSON")

    if 'on' in request.json and not isinstance(request.json['on'], bool):
        return _invalid_request_response('"on" must be true or false')

    if 'on' in request.json:
        light['on'] = request.json['on']
        _update_light(light)

    return jsonify(_make_public_light(light))


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not Found'}), 404)


@auth.get_password
def get_password(username):
    # TODO: This should _really_ get some proper handling
    if username == 'elva':
        return 'tolv'

    return None


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized'}), 401)


def _make_public_light(light):
    new_light = {}
    for field in light:
        if field == 'id':
            new_light['uri'] = url_for('get_light',
                                       type=light['type'],
                                       id=light['id'],
                                       _external=True)
        else:
            new_light[field] = light[field]

    return new_light


def _get_light(type, id):
    light = None
    if type == 'telldus':
        light = telldus.get_device(id)
    elif type == 'hue':
        light = hue.get_device(id)

    return light


def _update_light(light):
    if light['type'] == 'telldus':
        telldus.update_device(light)
    elif light['type'] == 'hue':
        hue.update_device(light)


def _invalid_request_response(description):
    return jsonify({
        'error': 'Invalid Request',
        'description': description
    }), 400


if __name__ == '__main__':
    cert = sys.argv[1]
    key = sys.argv[2]
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.load_cert_chain(cert, key)
    app.run(debug=True, host='0.0.0.0', ssl_context=context)
